#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = global-statement

# -------
# imports
# -------

from typing import IO, List

# lazy cycle-length cache
cache = {1: 1}

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert j > 0 and i > 0

    global cache

    # flipped range
    if i > j:
        i, j = j, i

    # range optimization from class notes
    m = j // 2 + 1
    if i < m < j:
        i = m

    maximum = 1
    for n in range(i, j + 1):
        # check cache
        cycle_length = cache.get(n, -1)
        # cache miss
        if cycle_length == -1:
            cycle_length = get_cycle_length(n)
            cache.update({n: cycle_length})

        # check, update maximum
        if cycle_length > maximum:
            maximum = cycle_length

    assert maximum > 0
    return maximum


# compute collatz cycle length
def get_cycle_length(n: int) -> int:
    assert n > 0

    global cache

    i = 1
    while n != 1:
        # use cycle length cache
        partial_cycle_length = cache.get(n, -1)
        if partial_cycle_length != -1:
            return i + partial_cycle_length - 1
        # cache miss
        i += 1
        if n % 2 == 0:
            n //= 2
        else:
            # odd case optimization from class notes
            n = n + n // 2 + 1
            i += 1

    assert i > 0
    return i


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
